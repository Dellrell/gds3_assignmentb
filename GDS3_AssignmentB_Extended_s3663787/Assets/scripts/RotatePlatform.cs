﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlatform : MonoBehaviour {
    public bool reverseSpin;
    public float spinRate = 10;
    public enum RotationAxis {xAxis, yAxis, zAxis};
    public RotationAxis rotationAxis;

    private Vector3 spinAxis;
    

	// Use this for initialization
	void Start () {
         if (reverseSpin)
            spinRate *= -1;
        if (rotationAxis == RotationAxis.xAxis) {
            spinAxis = new Vector3(spinRate * Time.deltaTime, 0, 0);
        }
        else if (rotationAxis == RotationAxis.yAxis) {
            spinAxis = new Vector3(0, spinRate * Time.deltaTime, 0);
        }
        else {
            spinAxis = new Vector3(0, 0, spinRate * Time.deltaTime);
        }

        }
	
	// Update is called once per frame
	void Update () {        
        transform.Rotate(spinAxis); 
	}
}
