﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour {

    public Slider mouseSX;
    public Slider mouseSY;
    public Toggle yAxisInv; 

    public static float hSliderValueX = 5.0f;
    public static float hSliderValueY = 5.0f;
    public static bool invertY = false;

    public void OnGUI() {
        hSliderValueX = mouseSX.value;
        hSliderValueY = mouseSY.value;
        invertY = yAxisInv;
    }

    private void Update() {
        /*if(invertY == false) {
            Debug.Log("no, I'm not a weirdo");
        } else {
            Debug.Log("yes, I'm Max");
        }*/
    }
}
