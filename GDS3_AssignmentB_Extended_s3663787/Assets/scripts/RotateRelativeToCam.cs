﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRelativeToCam : MonoBehaviour
{

    public float maxRotation = 30f;
    [Range(0.0f, 1.0f)]
    public float rotationSpeed = 0.01f;

    private int xAxisRotation = 0;
    private int zAxisRotation = 0;
    Transform mainCam;

    // Use this for initialization
    void Start()
    {
        mainCam = Camera.main.transform;
    }
    
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            xAxisRotation = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            xAxisRotation = -1;
        }
        else
        {
            xAxisRotation = 0;
        }

        if (Input.GetKey(KeyCode.D))
        {
            zAxisRotation = -1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            zAxisRotation = 1;
        }
        else
        {
            zAxisRotation = 0;
        }


        Quaternion targetRotationX = Quaternion.AngleAxis(xAxisRotation * maxRotation, mainCam.right);
        Quaternion targetRotationZ = Quaternion.AngleAxis(zAxisRotation * maxRotation, mainCam.forward);

        Quaternion targetRotation = targetRotationX * targetRotationZ;

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed);
        float diff = Quaternion.Angle(transform.rotation, targetRotation);
        if (diff < 0.1)
            transform.rotation = targetRotation;

    }
}
