﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEnd : MonoBehaviour {
    public GameObject[] level;
    public int currentLevel = 0;

    private void OnCollisionEnter(Collision collision) {
        if (collision.collider.tag == "Finish") {
            level[currentLevel].SetActive(false);

            currentLevel++;
            if (currentLevel > 4)
                currentLevel = 0;

            transform.position = new Vector3(-11, 6, -11);
            level[currentLevel].SetActive(true);
        } 
    }
}
