﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour {

    public float minZoom = -25;
    public float maxZoom = -3;
    public float sensitivity = 5.0f;

    Vector3 currentPosition;
    float currentZoom;

	// Use this for initialization
	void Start () {
        currentPosition = transform.localPosition;
        currentZoom = currentPosition.z;
		
	}
	
	// Update is called once per frame
	void Update () {
        currentZoom += Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        currentZoom = Mathf.Clamp(currentZoom, minZoom, maxZoom);

        transform.localPosition = new Vector3(0, 0, currentZoom);
	}
}
