﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jump : MonoBehaviour {

    public float jumpForce = 6f;
    Rigidbody rb;
    private bool isGrounded;
    public static bool canJump = false;
    GameObject powerUp;
    ParticleSystem part;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        part = GetComponent<ParticleSystem>();

    }

    // called once the collider/rigidbody touches a collider/rigidbodys
    void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;

    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Jump") {
            powerUp = other.gameObject;
            canJump = true;
        }
    }

    // Update is called once per frame
    void Update () {
        if (canJump) {
            if (!part.isPlaying)
                part.Play();
            if (Input.GetKeyDown(KeyCode.Space) && isGrounded) {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                isGrounded = false;
                canJump = false;
                part.Stop();
            } 
        }

        if (powerUp != null) {
            if (canJump) {
                powerUp.SetActive(false);
            } else {
                powerUp.SetActive(true);
            }
        }
	}
}
