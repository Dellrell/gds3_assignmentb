﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {

    public float jumpForce = 4f;
    Rigidbody rb;
    private bool isGrounded;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}

    // called once the collider/rigidbody touches a collider/rigidbodys
    private void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }		
	}
}
