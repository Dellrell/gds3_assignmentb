﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitalCamera : MonoBehaviour {

    public Transform target;
    public float xSensitivity = 5f;
    public float ySensitivity = 5f;
    
    public float minimunY = -20f;
    public float maximiumY = 80f;
    public bool invertY;

    float horizontalInput = 0f;
    float verticalInput = 0f;

    Vector3 targetPosition;
    Vector3 currentRotation;

	// Use this for initialization
	void Start () {
        currentRotation = transform.eulerAngles;
        horizontalInput = currentRotation.y;
        verticalInput = currentRotation.x;
	}
	
	// FixedUpdate is called on a static timer, possibly multiple times per frame. Movement calcs do not need *Time.DeltaTime because of the fixed timing
	void FixedUpdate () {
        float invertFlag = 1f;
        if (invertY)
            invertFlag = -1f;

        horizontalInput += Input.GetAxis("Mouse X") * xSensitivity;
        verticalInput -= Input.GetAxis("Mouse Y") * ySensitivity * invertFlag;
        verticalInput = Mathf.Clamp(verticalInput, minimunY, maximiumY);

        Quaternion rotation = Quaternion.Euler(verticalInput, horizontalInput, 0f);

        transform.rotation = rotation;

        targetPosition = target.position;
        transform.position = Vector3.Lerp(transform.position, targetPosition, 0.1f);		
	}
}
