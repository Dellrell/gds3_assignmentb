﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactParticles : MonoBehaviour {

    public ParticleSystem partPrefab;
    public Transform partTrans;

    private void OnCollisionEnter(Collision collision)
    {
        if (!partPrefab.IsAlive())
        {
            ContactPoint contact = collision.contacts[0];
            Vector3 pos = contact.point;
            partTrans.position = pos;
            partTrans.rotation = Quaternion.identity;
            partPrefab.Emit(10);
        }
       
    }
    
}
